# ENDPOINTS disponibles : 

# Table of Contents
1. [Utilisateur](#utilisateur)
   1. [Tous les utilisateurs](#tous-les-utilisateurs)
   2. [Utilisateur par son ID](#utilisateur-par-son-ID)
   3. [Utilisateur par email mot de passe](#utilisateur-par-email-mot-de-passe)
   4. [Créer un utilisateur](#créer-un-utilisateur)
   5. [Mettre à jour un utilisateur](#mettre-à-jour-un-utilisateur)
   6. [Supprimer un utilisateur](#supprimer-un-utilisateur)
2. [Championship](#Championship)
   1. [Tous les championnats](#tous-les-championnats)
   2. [Championnat par son ID](#championnat-par-son-ID)
   3. [Créer un championnat](#créer-un-championnat)
   4. [Mettre à jour un championnat](#mettre-à-jour-un-championnat)
   5. [Supprimer un championnat](#supprimer-un-championnat)
3. [Days](#days)
   1. [Toutes les journées](#toutes-les-journées)
   2. [Journée par son ID](#journée-par-son-ID)
   3. [Liste de journées par id championnat](#liste-de-journées-par-id-championnat)
   4. [Créer une journée](#créer-une-journée)
   5. [Mettre à jour une journée](#mettre-à-jour-une-journée)
   6. [Supprimer une journée](#supprimer-une-journée)
4. [Teams](#teams)
   1. [Toutes les équipes](#toutes-les-équipes)
   2. [Equipe par son ID](#equipe-par-son-ID)
   3. [Liste des équipes par id championnat](#liste-de-équipes-par-id-championnat)
   4. [Créer une équipe](#créer-une-équipe)
   5. [Mettre à jour une équipe](#mettre-à-jour-une-équipe)
   6. [Supprimer une équipe](#supprimer-une-équipe)
5. [Games](#games)
   1. [Tous les jeux](#tous-les-jeux)
   2. [Jeu par son ID](#jeu-par-son-id)
   3. [Liste des jeux par id championnat](#liste-des-jeux-par-id-championnat)
   4. [Liste des jeux par id journée](#liste-des-jeux-par-id-journée)
   5. [Créer un jeux pour une journée](#créer-un-jeux-pour-une-journée)


## Utilisateur 
`/api/users`

### Tous les utilisateurs
route : `/` \
method : `GET` \
return : `List<User>`

### Utilisateur par son ID
route : `/{user_id}` \
method : `GET` \
throw : `ResponseStatusException`
return : `User`

### Utilisateur par email mot de passe
route : `/user` \
method : `POST` \
body request (JSON) :
```JSON 
{
	"email": "e@e.e",
	"password": "test-test"
}
```
throw : `ResponseStatusException`
return : `User`

### Créer un utilisateur
route : `/` \
body request (JSON) : 
```JSON 
{
	"firstName": "test",
	"lastName": "test",
	"email": "e@e.e",
	"password": "test-test"
}
```
method : `POST` \
return : `ResponseEntity<User>`

### Mettre à jour un utilisateur
route : `/{user_id}` \
body request (JSON) :
```JSON 
{
  "firstName": "test modif",
  "lastName": "test modif",
  "email": "modif@e.e",
  "password": "test-modif"
}
```
method : `PUT` \
throw : `ResponseStatusException` \
return : `ResponseEntity<User>`

### Supprimer un utilisateur
route : `/{user_id}` \
method : `DELETE` \
return : `void`

## Championship
`/api/championships`

### Tous les championnats
route : `/` \
method : `GET` \
return : `List<ChampionShip>`

### Championnat par son ID
route : `/{championship_id}` \
method : `GET` \
throw : `ResponseStatusException`
return : `Championship`

### Créer un championnat
route : `/` \
body request (JSON) :
```JSON 
{
  "name": "Championnat de basket",
  "startDate": "2024-01-01",
  "endDate": "2024-31-12",
  "wonPoints": 100,
  "lostPoints": 30,
  "drawPoints": 33
}
```
method : `POST` \
return : `ResponseEntity<Championship>`

### Mettre à jour un championnat
route : `/{championship_id}` \
body request (JSON) :
```JSON 
{
  "name": "Championnat de basket modifié",
  "startDate": "2024-12-05",
  "endDate": "2024-12-05",
  "wonPoints": 99,
  "lostPoints": 31,
  "drawPoints": 30
}
```
method : `PUT` \
return : `ResponseEntity<Championship>`

### Supprimer un championnat
route : `/{championship_id}` \
method : `DELETE` \
return : `void`

## Days
`/api/days`

### Toutes les journées
route : `/` \
method : `GET` \
return : `List<Day>`

### Journée par son ID
route : `/{day_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `Day`

### Liste de journées par id championnat
route : `/by-championship/{championship_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `List<Day>`

### Créer une journée
route : `/` \
body request (JSON) :
```JSON 
{
  "number": "journée test",
  "championship": {
    "id": 1
  }
}
```
method : `POST` \
return : `ResponseEntity<Day>`

### Mettre à jour une journée
route : `/{day_id}` \
body request (JSON) :
```JSON 
{
   "number": "journée test modifié",
   "championship": {
      "id": 1
   }
}
```
method : `PUT` \
return : `ResponseEntity<Day>`

### Supprimer une journée
route : `/{day}` \
method : `DELETE` \
return : `void`

## Teams
`/api/days`

### Toutes les équipes
route : `/` \
method : `GET` \
return : `List<Team>`

### Equipe par son ID
route : `/{day_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `Team`

### Liste des équipes par id championnat
route : `/by-championship/{championship_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `List<Team>`

### Créer une équipe
route : `/` \
body request (JSON) :
```JSON 
{
   "nom": "team une",
   "createdAt": "2024-06-18"
}
```
method : `POST` \
return : `ResponseEntity<Team>`

### Mettre à jour une équipe
route : `/{team_id}` \
body request (JSON) :
```JSON 
{
   "nom": "team modifié",
   "createdAt": "2024-06-18"
}
```
method : `PUT` \
return : `ResponseEntity<Day>`

### Supprimer une équipe
route : `/{day}` \
method : `DELETE` \
return : `void`

## Games
`/api/games`

### Tous les jeux
route : `/` \
method : `GET` \
return : `List<Game>`

### Jeu par son ID
route : `/{game_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `Game`

### Liste des jeux par id championnat
route : `/by-championship/{championship_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `List<Game>`

### Liste des jeux par id journée
route : `/by-day/{day_id}` \
method : `GET` \
throw : `ResponseStatusException` \
return : `List<Game>`

### Créer un jeux pour une journée
route : `/` \
body request (JSON) :
```JSON 
{
   "team1Point": 100,
   "team2Point": 0,
   "day": {
      "id": 6
   },
   "team1": {
      "id": 1
   },
   "team2": {
      "id": 2
   }
}
```
method : `POST` \
return : `ResponseEntity<Game>`

### Mettre à jour un jeu
route : `/{game_id}` \
body request (JSON) :
```JSON 
{
   "team1Point": 1000,
   "team2Point": 11,
   "day": {
      "id": 2
   },
   "team1": {
      "id": 3
   },
   "team2": {
      "id": 4
   }
}
```
method : `PUT` \
return : `ResponseEntity<Game>`

### Supprimer une équipe
route : `/{game}` \
method : `DELETE` \
return : `void`