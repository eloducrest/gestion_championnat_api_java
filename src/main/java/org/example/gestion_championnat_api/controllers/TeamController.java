package org.example.gestion_championnat_api.controllers;

import jakarta.validation.Valid;
import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.Team;
import org.example.gestion_championnat_api.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/teams")
public class TeamController {

    private final TeamRepository teamRepository;

    @Autowired
    public TeamController(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    // Récupérer la liste des équipes
    // /api/teams/
    @GetMapping(value = "/")
    public List<Team> getAllTeams() {
        return teamRepository.findAll();
    }

    // Récupérer une journée par son id
    // /api/teams/{id_day}
    @GetMapping(value = "/{team}")
    public Team getTeamById(@PathVariable(name = "team", required = true) Team team) {
        if (team == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Equipe introuvable"
            );
        }
        return team;
    }

    // Récupérer la liste des équipes par ID championnat
    // /api/teams/{championship_id}
    @GetMapping(value = "/by-championship/{championship}")
    public List<Team> getTeamsByChampionshipId(@PathVariable(name = "championship", required = true) Championship championship) {
        if (championship == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Championnat requis pour cette requête"
            );
        }
        return teamRepository.findAllByChampionshipsContaining(championship);
    }

    // Créer une équipe
    // /api/teams/
    @PostMapping(value = "/")
    public ResponseEntity<Team> storeTeam(@Valid @RequestBody Team team) {
        teamRepository.save(team);
        return new ResponseEntity<>(team, HttpStatus.CREATED);
    }

    // Mettre à jour une équipe
    // /api/teams/{team_id}
    @PutMapping(value = "/{team}")
    public ResponseEntity<Team> updateTeam(@PathVariable(name = "team") Team team,
                                           @Valid @RequestBody Team teamUpdate, BindingResult bindingResult) {
        if (team == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Equipe introuvable");
        } else {
            if (bindingResult.hasErrors()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
            } else {
                teamUpdate.setId(team.getId());
                teamRepository.save(teamUpdate);
                return new ResponseEntity<>(team, HttpStatus.OK);
            }
        }
    }


    // TODO : voir le delete cascade
    // Supprimer une équipe
    // /api/teams/{team_id}
    @DeleteMapping("/{team}")
    public void deleteTeam(@PathVariable(name = "team", required = false) Team team) {
        if (team == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Equipe introuvable");
        } else {
            teamRepository.delete(team);
        }
    }

}
