package org.example.gestion_championnat_api.controllers;

import jakarta.validation.Valid;
import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.Day;
import org.example.gestion_championnat_api.models.Game;
import org.example.gestion_championnat_api.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/api/games")
public class GameController {

    private final GameRepository gameRepository;

    @Autowired
    public GameController(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    // Récupérer la liste des jeux
    // /api/games/
    @GetMapping(value = "/")
    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    // Récupérer un jeu par son id
    // /api/games/{id_game}
    @GetMapping(value = "/{game}")
    public Game getGameById(@PathVariable(name = "game", required = true) Game game) {
        if (game == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Jeu introuvable"
            );
        }
        return game;
    }

    // Récupérer la liste des jeux par ID championnat
    // /api/games/by-championship/{championship_id}
    @GetMapping(value = "/by-championship/{championship}")
    public List<Game> getGamesByChampionshipId(@PathVariable(name = "championship", required = true) Championship championship) {
        if (championship == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Championnat requis pour cette requête"
            );
        }
        return gameRepository.findAllByChampionship_Id(championship.getId());
    }

    // Récupérer la liste des jeux par ID journée
    // /api/games/by-day/{day_id}
    @GetMapping(value = "/by-day/{day}")
    public List<Game> getGamesByDayId(@PathVariable(name = "day", required = true) Day day) {
        if (day == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Journée requise pour cette requête"
            );
        }
        return gameRepository.findAllByDay(day);
    }

    // Créer un jeu pour une journée
    // /api/games/
    @PostMapping(value = "/")
    public ResponseEntity<Game> storeGame(@Valid @RequestBody Game game) {
        gameRepository.save(game);
        return new ResponseEntity<>(game, HttpStatus.CREATED);
    }

    // Mettre à jour un jeu
    // /api/games/{game_id}
    @PutMapping(value = "/{game}")
    public ResponseEntity<Game> updateGame(@PathVariable(name = "game") Game game,
                                           @Valid @RequestBody Game gameUpdate, BindingResult bindingResult) {
        if (game == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Jeu introuvable");
        } else {
            if (bindingResult.hasErrors()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
            } else {
                gameUpdate.setId(game.getId());
                gameRepository.save(gameUpdate);
                return new ResponseEntity<>(game, HttpStatus.OK);
            }
        }
    }


    // TODO : voir le delete cascade
    // Supprimer un jeu
    // /api/games/{game_id}
    @DeleteMapping("/{game}")
    public void deleteGame(@PathVariable(name = "game", required = false) Game game) {
        if (game == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Jeu introuvable");
        } else {
            gameRepository.delete(game);
        }
    }

}
