package org.example.gestion_championnat_api.controllers;

import jakarta.validation.Valid;
import org.example.gestion_championnat_api.models.User;
import org.example.gestion_championnat_api.repositories.UserRepository;
import org.example.gestion_championnat_api.requests.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // Récupérer la liste des utilisateurs
    @GetMapping("/") // /api/users/
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    // Récupérer un utilisateur par son id
    @GetMapping("/{user}") // /api/users/{id_user}
    public User getUserById(@PathVariable(name = "user", required = true) User user) {
        if (user == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Utilisateur introuvable"
            );
        }
        return user;
    }

    // Récupérer un utilisateur par son email et mot de passe
    @PostMapping("/user") // /api/users/user?email=&password=
    public Optional<User> getUserByEmailPassword(@RequestBody LoginRequest loginRequest) {
        String email = loginRequest.getEmail();
        String password = loginRequest.getPassword();

        Optional<User> user = userRepository.findByEmailAndPassword(email, password);
        if (user.isEmpty()) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Utilisateur introuvable"
            );
        }
        return user;
    }

    // Créer un nouvel utilisateur
    @PostMapping(value = "/") // /api/users/
    public ResponseEntity<User> storeUser(@Valid @RequestBody User user) {
        userRepository.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    // Mettre à jour un utilisateur
    @PutMapping(value = "/{user}") // /api/users/{id_user}
    public ResponseEntity<User> updateUser(@PathVariable(name = "user", required = false) User user,
                                           @Valid @RequestBody User userUpdate, BindingResult bindingResult) {
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User introuvable");
        } else {
            if (bindingResult.hasErrors()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
            } else {
                userUpdate.setId(user.getId());
                userRepository.save(userUpdate);
                return new ResponseEntity<>(user, HttpStatus.OK);
            }
        }
    }

    // Supprimer un utilisateur
    @DeleteMapping("/{user}")  // /api/users/{id_user}
    public void deleteUser(@PathVariable(name = "user", required = false) User user) {
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidat introuvable");
        } else {
            userRepository.delete(user);
        }
    }
}
