package org.example.gestion_championnat_api.controllers;

import jakarta.validation.Valid;
import org.example.gestion_championnat_api.exceptions.ForeignKeyConstraintException;
import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.Day;
import org.springframework.dao.DataIntegrityViolationException;
import org.example.gestion_championnat_api.repositories.DayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/days")
public class DayController {

    private static final Logger log = LoggerFactory.getLogger(DayController.class);
    private final DayRepository dayRepository;

    @Autowired
    public DayController(DayRepository dayRepository) {
        this.dayRepository = dayRepository;
    }

    // Récupérer la liste des journées
    // /api/days/
    @GetMapping(value = "/")
    public List<Day> getAllDays() {
        return dayRepository.findAll();
    }

    // Récupérer une journée par son id
    // /api/days/{id_day}
    @GetMapping(value = "/{day}")
    public Day getDayById(@PathVariable(name = "day", required = true) Day day) {
        if (day == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Journée introuvable"
            );
        }
        return day;
    }

    // Récupérer la liste des journées par ID championnat
    // /api/days/{championship_id}
    @GetMapping(value = "/by-championship/{championship}")
    public List<Day> getDayByChampionshipId(@PathVariable(name = "championship", required = true) Championship championship) {
        if (championship == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Championnat requis pour cette requête"
            );
        }
        return dayRepository.findAllByChampionship(championship);
    }

    // Créer une journée pour un championnat
    // /api/days/
    @PostMapping(value = "/")
    public ResponseEntity<Day> storeDay(@Valid @RequestBody Day day) {
        try {
            dayRepository.save(day);
            return new ResponseEntity<>(day, HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            throw new ForeignKeyConstraintException("Le championnat n'existe pas", e);
        }
    }

    // Mettre à jour une journée
    // /api/days/{day_id}
    @PutMapping(value = "/{day}")
    public ResponseEntity<Day> updateDay(@PathVariable(name = "day") Day day,
                                                           @Valid @RequestBody Day dayUpdate, BindingResult bindingResult) {
        if (day == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Journée introuvable");
        } else {
            if (bindingResult.hasErrors()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
            } else {
                dayUpdate.setId(day.getId());
                dayRepository.save(dayUpdate);
                return new ResponseEntity<>(day, HttpStatus.OK);
            }
        }
    }

    // Supprimer une journée
    // /api/days/{day_id}
    @DeleteMapping("/{day}")
    public void deleteDay(@PathVariable(name = "day", required = false) Day day) {
        if (day == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Journée introuvable");
        } else {
            dayRepository.delete(day);
        }
    }

}
