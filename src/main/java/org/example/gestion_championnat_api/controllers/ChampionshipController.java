package org.example.gestion_championnat_api.controllers;

import jakarta.validation.Valid;
import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.User;
import org.example.gestion_championnat_api.repositories.ChampionshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/championships")
public class ChampionshipController {

    private final ChampionshipRepository championshipRepository;

    @Autowired
    public ChampionshipController(ChampionshipRepository championshipRepository) {
        this.championshipRepository = championshipRepository;
    }

    // Récupérer la liste des championnats
    // /api/championships/
    @GetMapping("/")
    public List<Championship> getAllChampionships() {
        return championshipRepository.findAll();
    }

    // Récupérer un championnnat par son id
    // /api/championships/{id_championship}
    @GetMapping("/{championship}")
    public Championship getChampionshipById(@PathVariable(name = "championship", required = true) Championship championship) {
        if (championship == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Championnat introuvable"
            );
        }
        return championship;
    }

    // Créer un nouveau championnat
    // /api/championships/
    @PostMapping(value = "/") // /api/users/
    public ResponseEntity<Championship> storeChampionship(@Valid @RequestBody Championship championship) {
        championshipRepository.save(championship);
        return new ResponseEntity<>(championship, HttpStatus.CREATED);
    }

    // Mettre à jour un championnat
    // /api/championships/{id_championship}
    @PutMapping(value = "/{championship}")
    public ResponseEntity<Championship> updateChampionship(@PathVariable(name = "championship") Championship championship,
                                                           @Valid @RequestBody Championship championshipUpdate, BindingResult bindingResult) {
        if (championship == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Championnat introuvable");
        } else {
            if (bindingResult.hasErrors()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
            } else {
                championshipUpdate.setId(championship.getId());
                championshipRepository.save(championshipUpdate);
                return new ResponseEntity<>(championship, HttpStatus.OK);
            }
        }
    }

    // Supprimer un championnat
    // /api/championships/{id_championship}
    @DeleteMapping("/{championship}")
    public void deleteChampionship(@PathVariable(name = "championship", required = false) Championship championship) {
        if (championship == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Championnat introuvable");
        } else {
            championshipRepository.delete(championship);
        }
    }
}
