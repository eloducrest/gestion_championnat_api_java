package org.example.gestion_championnat_api.repositories;

import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.Day;
import org.example.gestion_championnat_api.models.Game;
import org.example.gestion_championnat_api.models.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends CrudRepository<Game, Long> {

    @Override
    List<Game> findAll();

    @Query("SELECT g FROM Game g WHERE g.day.championship.id = :championshipId")
    List<Game> findAllByChampionship_Id(Long championshipId);

    List<Game> findAllByDay(Day day);
}
