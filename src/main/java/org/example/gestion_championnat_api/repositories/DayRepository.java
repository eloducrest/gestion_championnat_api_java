package org.example.gestion_championnat_api.repositories;

import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.Day;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DayRepository extends CrudRepository<Day, Long> {

    @Override
    List<Day> findAll();

    List<Day> findAllByChampionship(Championship Championship);
}
