package org.example.gestion_championnat_api.repositories;

import org.example.gestion_championnat_api.models.Championship;
import org.example.gestion_championnat_api.models.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends CrudRepository<Team, Long> {

    @Override
    List<Team> findAll();

    List<Team> findAllByChampionshipsContaining(Championship Championship);
}
