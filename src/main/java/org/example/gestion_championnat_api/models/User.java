package org.example.gestion_championnat_api.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "firstName")
    @NotNull(message = "Le champ Prénom ne peut pas être null")
    @NotBlank(message = "Le champ Prénom ne peut pas être vide")
    private String firstName;

    @Column(name = "lastName")
    @NotNull(message = "Le champ NOM ne peut pas être null")
    @NotBlank(message = "Le champ NOM ne peut pas être vide")
    private String lastName;

    @Column(name = "email", unique = true)
    @NotNull(message = "Le champ email ne peut pas être null")
    @NotBlank(message = "Le champ email ne peut pas être vide")
    @Email(message = "Format de l'email invalide")
    private String email;

    @Column(name = "password")
    @Size(min = 8, message = "Le mot de passe doit contenir au minimum 8 charactères")
    @NotNull(message = "Le champ mot de passe ne peut pas être null")
    @NotBlank(message = "Le champ mot de passe ne peut pas être vide")
    private String password;

    @Column(name = "createdAt")
    @Temporal(value = TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate createdAt;

    public User() {
        this.createdAt = LocalDate.now();
    }

    public User(Long id, String firstName, String lastName, String email, String password, LocalDate createdAt) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull(message = "Le champ Prénom ne peut pas être null") @NotBlank(message = "Le champ Prénom ne peut pas être vide") String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NotNull(message = "Le champ Prénom ne peut pas être null") @NotBlank(message = "Le champ Prénom ne peut pas être vide") String firstName) {
        this.firstName = firstName;
    }

    public @NotNull(message = "Le champ NOM ne peut pas être null") @NotBlank(message = "Le champ NOM ne peut pas être vide") String getLastName() {
        return lastName;
    }

    public void setLastName(@NotNull(message = "Le champ NOM ne peut pas être null") @NotBlank(message = "Le champ NOM ne peut pas être vide") String lastName) {
        this.lastName = lastName;
    }

    public @NotNull(message = "Le champ email ne peut pas être null") @NotBlank(message = "Le champ email ne peut pas être vide") @Email(message = "Format de l'email invalide") String getEmail() {
        return email;
    }

    public void setEmail(@NotNull(message = "Le champ email ne peut pas être null") @NotBlank(message = "Le champ email ne peut pas être vide") @Email(message = "Format de l'email invalide") String email) {
        this.email = email;
    }

    public @NotNull(message = "Le champ mot de passe ne peut pas être null") @NotBlank(message = "Le champ mot de passe ne peut pas être vide") @Size(min = 8, message = "Le mot de passe doit contenir au minimum 8 charactères") String getPassword() {
        return password;
    }

    public void setPassword(@NotNull(message = "Le champ mot de passe ne peut pas être null") @NotBlank(message = "Le champ mot de passe ne peut pas être vide") @Size(min = 8, message = "Le mot de passe doit contenir au minimum 8 charactères") String password) {
        this.password = password;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }
}
