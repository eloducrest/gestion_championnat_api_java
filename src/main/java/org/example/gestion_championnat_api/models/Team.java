package org.example.gestion_championnat_api.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "teams")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotNull(message = "Le champ nom ne peut pas être null")
    @NotBlank(message = "Le champ nom ne peut pas être vide")
    private String nom;

    @Column(name = "createdAt")
    @Temporal(value = TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate createdAt;

    @OneToMany(mappedBy = "teamOne", fetch = FetchType.LAZY)
    @JsonManagedReference
    private Set<Game> gamesAsTeam1;

    @OneToMany(mappedBy = "teamTwo", fetch = FetchType.LAZY)
    @JsonManagedReference
    private Set<Game> gamesAsTeam2;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "teams")
    @JsonIgnore
    private Set<Championship> championships = new HashSet<>();

    public Team() {
        this.createdAt = LocalDate.now();
    }

    public Team(Long id, String nom, LocalDate createdAt) {
        this.id = id;
        this.nom = nom;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull(message = "Le champ nom ne peut pas être null") @NotBlank(message = "Le champ nom ne peut pas être vide") String getNom() {
        return nom;
    }

    public void setNom(@NotNull(message = "Le champ nom ne peut pas être null") @NotBlank(message = "Le champ nom ne peut pas être vide") String nom) {
        this.nom = nom;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public Set<Game> getGamesAsTeam1() {
        return gamesAsTeam1;
    }

    public void setGamesAsTeam1(Set<Game> gamesAsTeam1) {
        this.gamesAsTeam1 = gamesAsTeam1;
    }

    public Set<Game> getGamesAsTeam2() {
        return gamesAsTeam2;
    }

    public void setGamesAsTeam2(Set<Game> gamesAsTeam2) {
        this.gamesAsTeam2 = gamesAsTeam2;
    }

    public Set<Championship> getChampionships() {
        return championships;
    }

    public void setChampionships(Set<Championship> championships) {
        this.championships = championships;
    }
}
