
package org.example.gestion_championnat_api.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "championships")
public class Championship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotNull(message = "Le champ nom ne peut pas être null")
    @NotBlank(message = "Le champ nom ne peut pas être vide")
    private String name;

    @Column(name = "startDate")
    @NotNull(message = "Le champ date de début ne peut pas être null")
    @Temporal(value = TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @Column(name = "endDate")
    @NotNull(message = "Le champ date de fin ne peut pas être null")
    @Temporal(value = TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Column(name = "wonPoints")
    private Integer wonPoints;

    @Column(name = "lostPoints")
    private Integer lostPoints;

    @Column(name = "drawPoints")
    private Integer drawPoints;

    @OneToMany(mappedBy = "championship", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Day> days = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(
            name = "team_championships",
            joinColumns = @JoinColumn(name = "championship_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id")
    )
    @JsonIgnore
    private Set<Team> teams = new HashSet<>();

    public Championship() {
    }

    public Championship(Long id, String name, LocalDate startDate, LocalDate endDate, Integer wonPoints, Integer lostPoints, Integer drawPoints) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.wonPoints = wonPoints;
        this.lostPoints = lostPoints;
        this.drawPoints = drawPoints;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public @NotNull(message = "Le champ nom ne peut pas être null") @NotBlank(message = "Le champ nom ne peut pas être vide") String getName() {
        return name;
    }

    public void setName(@NotNull(message = "Le champ nom ne peut pas être null") @NotBlank(message = "Le champ nom ne peut pas être vide") String name) {
        this.name = name;
    }

    public @NotNull(message = "Le champ date de début ne peut pas être null") LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(@NotNull(message = "Le champ date de début ne peut pas être null") LocalDate startDate) {
        this.startDate = startDate;
    }

    public @NotNull(message = "Le champ date de fin ne peut pas être null") LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(@NotNull(message = "Le champ date de fin ne peut pas être null") LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getWonPoints() {
        return wonPoints;
    }

    public void setWonPoints(Integer wonPoints) {
        this.wonPoints = wonPoints;
    }

    public Integer getLostPoints() {
        return lostPoints;
    }

    public void setLostPoints(Integer lostPoints) {
        this.lostPoints = lostPoints;
    }

    public Integer getDrawPoints() {
        return drawPoints;
    }

    public void setDrawPoints(Integer drawPoints) {
        this.drawPoints = drawPoints;
    }

    public Set<Day> getDays() {
        return days;
    }

    public void setDays(Set<Day> days) {
        this.days = days;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }
}
