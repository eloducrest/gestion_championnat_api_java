package org.example.gestion_championnat_api.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "games")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "team1_point")
    private Integer team1Point;

    @Column(name = "team2_point")
    private Integer team2Point;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Team teamOne;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Team teamTwo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Day day;

    public Game() {}

    public Game(Long id, Integer team1Point, Integer team2Point) {
        this.id = id;
        this.team1Point = team1Point;
        this.team2Point = team2Point;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTeam1Point() {
        return team1Point;
    }

    public void setTeam1Point(Integer team1Point) {
        this.team1Point = team1Point;
    }

    public Integer getTeam2Point() {
        return team2Point;
    }

    public void setTeam2Point(Integer team2Point) {
        this.team2Point = team2Point;
    }

    public Team getTeam1() {
        return teamOne;
    }

    public void setTeam1(Team team1) {
        this.teamOne = team1;
    }

    public Team getTeam2() {
        return teamTwo;
    }

    public void setTeam2(Team team2) {
        this.teamTwo = team2;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }
}
