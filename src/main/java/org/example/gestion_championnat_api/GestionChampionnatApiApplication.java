package org.example.gestion_championnat_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionChampionnatApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionChampionnatApiApplication.class, args);
    }

}
